
		<div class="row">
        
          <section class="panel">

                <header class="panel-heading">
                	<div class="row">
	                	<div class="col-md-6">
		                    <h2 class="panel-title"><?php echo $title;?></h2>
		                </div>
		                <div class="col-md-6">
                        	<a href="<?php echo site_url();?>accounts/salary-data" class="btn btn-sm btn-info pull-right">Back to personnel</a>
		                </div>
	                </div>
                </header>
                <div class="panel-body">
                    
                    <div class="row">
                    	<div class="col-md-12">
                        	<?php
                            	$success = $this->session->userdata('success_message');
                            	$error = $this->session->userdata('error_message');
								
								if(!empty($success))
								{
									echo '
										<div class="alert alert-success">'.$success.'</div>
									';
									
									$this->session->unset_userdata('success_message');
								}
								
								if(!empty($error))
								{
									echo '
										<div class="alert alert-danger">'.$error.'</div>
									';
									
									$this->session->unset_userdata('error_message');
								}
                    
								$validation_errors = validation_errors();
								
								if(!empty($validation_errors))
								{
									echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
								}
								
							?>
                            <div class="row">
                                <div class="col-md-6">
                                </div>
                                <div class="col-md-6">
                                    <a href="<?php echo site_url();?>accounts/payroll/view-payslip/<?php echo $personnel_id;?>" class="btn btn-sm btn-warning pull-right" target="_blank">View payslip</a>
                                </div>
                            </div>
                        	
                            <div class="row">
                                <div class="col-md-6">
                                    <section class="panel panel-featured panel-featured-info">
                                        <header class="panel-heading">						
                                            <h2 class="panel-title">Payments</h2>
                                        </header>
                                        <div class="panel-body">
                                        	<form action="<?php echo site_url("accounts/payroll/edit-personnel-payments/".$personnel_id);?>" method="post">
                                            <table class='table table-striped table-hover table-condensed'>
                                                
                                            <?php
                                            
											$total_payments = 0;
											
                                            if($payments->num_rows() > 0)
                                            {
                                                foreach ($payments->result() as $row2)
                                                {
                                                    $payment_id = $row2->payment_id;
                                                    $payment_name = $row2->payment_name;
                                                    
                                                    if($personel_payments->num_rows() > 0){
                                                        foreach($personel_payments->result() as $allow){
                                                            $id = $allow->id;
                                                            
                                                            if($id == $payment_id){
                                                                $amount = $allow->amount;
                                                                break;
                                                            }
                                                    
                                                            else{
                                                                $amount = 0;
                                                            }
                                                        }
                                                    }
                                                    
                                                    else{
                                                        $amount = 0;
                                                    }
                                                    $total_payments += $amount;
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $payment_name;?></td>
                                                        <td><input type='text' name='personnel_payment_amount<?php echo $payment_id;?>' value='<?php echo $amount;?>' class="form-control"></td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            ?>
                                                <tr>
                                                    <th>TOTAL</th>
                                                    <th><?php echo number_format($total_payments, 2);?></th>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td><button class='btn btn-success' type='submit'><i class='fa fa-pencil'></i> Edit payments</button></td>
                                                </tr>
                                            </table>
                                            </form>
                                        </div>
                                    </section>
                                </div>
                                
                                <div class="col-md-6">
                                    <section class="panel panel-featured panel-featured-info">
                                        <header class="panel-heading">						
                                            <h2 class="panel-title">Deductions</h2>
                                        </header>
                                        <div class="panel-body">
                                        	<form action="<?php echo site_url("accounts/payroll/edit-personnel-other-deductions/".$personnel_id);?>" method="post">
                                            <table class='table table-striped table-hover table-condensed'>
                                            <?php
                                            
											$total_other_deductions = 0;
											
                                            if($other_deductions->num_rows() > 0)
                                            {
                                                foreach ($other_deductions->result() as $row2)
                                                {
                                                   	$other_deduction_id = $row2->other_deduction_id;
													$other_deduction_name = $row2->other_deduction_name;
													$other_deduction_taxable = $row2->other_deduction_taxable;
                                                    
                                                    if($personnel_other_deductions->num_rows() > 0){
                                                        foreach($personnel_other_deductions->result() as $allow){
                                                            $id = $allow->id;
                                                            
                                                            if($id == $other_deduction_id){
                                                                $amount = $allow->amount;
                                                                break;
                                                            }
                                                    
                                                            else{
                                                                $amount = 0;
                                                            }
                                                        }
                                                    }
                                                    
                                                    else{
                                                        $amount = 0;
                                                    }
                                                    $total_other_deductions += $amount;
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $other_deduction_name;?></td>
                                                        <td><input type='text' name='personnel_other_deduction_amount<?php echo $other_deduction_id;?>' value='<?php echo $amount;?>' class="form-control"></td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            ?>
                                                <tr>
                                                    <th>TOTAL</th>
                                                    <th><?php echo number_format($total_other_deductions, 2);?></th>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td><button class='btn btn-success' type='submit'><i class='fa fa-pencil'></i> Edit deductions</button></td>
                                                </tr>
                                            </table>
                                            </form>
                                        </div>
                                    </section>
                                </div>
                            </div>
                            
                            <div class="row">
                            	<div class="col-md-12">
                                    <section class="panel panel-featured panel-featured-info">
                                        <header class="panel-heading">						
                                            <h2 class="panel-title">Payment details</h2>
                                        </header>
                                        <div class="panel-body">
                                        	<form action="<?php echo site_url("accounts/payroll/edit-payment-details/".$personnel_id);?>" method="post">
                                            	<div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="col-lg-5 control-label">Account number: </label>
                                                            
                                                            <div class="col-lg-7">
                                                                <input type="text" class="form-control" name="personnel_account_number" placeholder="Account number" value="<?php echo $personnel_account_number;?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="col-lg-5 control-label">KRA number: </label>
                                                            
                                                            <div class="col-lg-7">
                                                                <input type="text" class="form-control" name="personnel_kra_pin" placeholder="KRA number" value="<?php echo $personnel_kra_pin;?>">
                                                                <input type="hidden" class="form-control" name="personnel_nhif_number" placeholder="NHIF number" value="<?php echo $personnel_nhif_number;?>">
                                                                <input type="hidden" class="form-control" name="personnel_nssf_number" placeholder="NSSF number" value="<?php echo $personnel_nssf_number;?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="col-md-12 center-align" style="margin-top:10px;">
                                                    	<button class='btn btn-success' type='submit'><i class='fa fa-pencil'></i> Edit payment details</button>
                                                    </div>
                                            	</div>
                                            </form>
                                        </div>
                                    </section>
                            	</div>
                            </div>
                            <!-- End payment details -->
                        </div>
                    </div>
                </div>
            </section>
        </div>